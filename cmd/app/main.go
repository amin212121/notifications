package main

import "redus-notifications/internal/app"

func main() {
	app.Run()
}
