package app

import (
	"context"
	"fmt"
	"github.com/gorilla/mux"
	"github.com/joho/godotenv"
	"github.com/spf13/viper"
	"log"
	"net/http"
	"os"
	"os/signal"
	delivery "redus-notifications/internal/delivery/http"
	"redus-notifications/internal/repository"
	"redus-notifications/internal/server"
	"redus-notifications/internal/service"
	"redus-notifications/pkg/DB/psql"
	"redus-notifications/pkg/FCM"
	"time"
)

func init() {
	if err := initConfig(); err != nil {
		log.Fatalf("error initializing configs: %s", err.Error())
	}

	if err := godotenv.Load(".env"); err != nil {
		log.Fatal("Error loading .env file")
	}
}

func Run() {
	firebaseFCM := FCM.Run()
	db, err := psql.NewPostgresDB(psql.Config{
		Host:     viper.GetString("db.host"),
		Port:     viper.GetString("db.port"),
		DBName:   viper.GetString("db.dbname"),
		SSLMode:  viper.GetString("db.sslmode"),
		Username: os.Getenv("DB_USER"),
		Password: os.Getenv("DB_PASSWORD"),
	})
	if err != nil {
		log.Fatalf("error initializing DB: %s", err.Error())
	}

	// repo layer has been injected dependency - PSQL, Firebase
	repos := repository.NewDeviceRepo(db, firebaseFCM)
	// next layer: services layer has been injected dependency - repos
	services := service.NewTokenService(repos)
	// next layer: handlers layer has been injected dependency - repository
	handlers := delivery.NewHandler(services)

	rootRouter := mux.NewRouter()
	publicAuthRouter := rootRouter.PathPrefix("/notifications").Subrouter()
	publicAuthRouter.HandleFunc("/device_token", handlers.AddDeviceToken).Methods(http.MethodPost)
	publicAuthRouter.HandleFunc("/device_token", handlers.RemoveDeviceToken).Methods(http.MethodDelete)
	publicAuthRouter.HandleFunc("/notify", handlers.Notify).Methods(http.MethodPost)

	srv := server.NewServer(server.Config{
		Port:               viper.GetString("http.port"),
		ReadTimeout:        viper.GetDuration("http.readTimeout"),
		WriteTimeout:       viper.GetDuration("http.writeTimeout"),
		MaxHeaderMegabytes: viper.GetInt("http.maxHeaderBytes"),
	}, rootRouter)

	go func() {
		if err := srv.Run(); err != nil {
			fmt.Println("error occurred while running http server: %s\n", err.Error())
		}
	}()

	sigChan := make(chan os.Signal)
	signal.Notify(sigChan, os.Interrupt)
	signal.Notify(sigChan, os.Kill)

	sig := <-sigChan
	fmt.Println("Receive terminate, graceful shutdown", sig)

	timeoutContext, _ := context.WithTimeout(context.Background(), 3*time.Second)
	srv.Stop(timeoutContext)
}

func initConfig() error {
	viper.AddConfigPath("configs")
	viper.SetConfigName("config")
	return viper.ReadInConfig()
}
