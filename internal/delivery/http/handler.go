package http

import (
	"fmt"
	"net/http"
	"redus-notifications/internal/service"
	"redus-notifications/pkg/JSON"
)

type TokenRequest struct {
	Userid string `json:"userid"`
	Token  string `json:"token"`
}

type Handler struct {
	services service.PostServiceI
}

func NewHandler(services service.PostServiceI) *Handler {
	return &Handler{
		services,
	}
}

func (a *Handler) AddDeviceToken(w http.ResponseWriter, r *http.Request) {
	data := &TokenRequest{}

	err := JSON.FromJSON(data, r.Body)
	if err != nil {
		w.WriteHeader(http.StatusBadRequest)
		return
	}
	fmt.Println(data)

	a.services.Add(data.Userid, data.Token)
}

func (a *Handler) RemoveDeviceToken(w http.ResponseWriter, r *http.Request) {
	a.services.Remove("999")
}

func (a *Handler) Notify(w http.ResponseWriter, r *http.Request) {
	a.services.Notify("22332", "2332")
}
