package repository

import (
	firebase "firebase.google.com/go"
	"github.com/jmoiron/sqlx"
)

type DeviceI interface {
	Add()
	Delete()
}

type DeviceRepo struct {
	DB *sqlx.DB
	FB *firebase.App
}

func NewDeviceRepo(DB *sqlx.DB, FB *firebase.App) *DeviceRepo {
	return &DeviceRepo{DB, FB}
}

func (d *DeviceRepo) Add(userId string, deviceToken string) error {
	q := "INSERT into users_device (user_id, device_token) values ($1, $2) RETURNING id"
	_, err := d.DB.Exec(q, userId, deviceToken)

	if err != nil {
		return err
	}

	return nil
}

func (d *DeviceRepo) Delete(userId string) error {
	q := "DELETE FROM users_device WHERE user_id=$1"
	_, err := d.DB.Exec(q, userId)

	if err != nil {
		return err
	}

	return nil
}
