package repository

import (
	"context"
	"firebase.google.com/go/messaging"
	"fmt"
	"log"
)

type NotifyI interface {
	Notify()
}


func (t *DeviceRepo) Notify(id string, data string) {
	// [START send_to_token_golang]
	// Obtain a messaging.Client from the App.
	ctx := context.Background()
	client, err := t.FB.Messaging(ctx)
	if err != nil {
		log.Fatalf("error getting Messaging client: %v\n", err)
	}

	// This registration token comes from the client FCM SDKs.
	registrationToken := "ebqhChTUQl6PljRH5lm7YL:APA91bFK51WFvARnsSoFHJ8bnZJHJD79nhOiaM0KikpAP4vKChlJ5oo0sTuF0pzjcn19X1IK9xCexbz_fx87oJJ8pIVw-Q5STextNzxzKEjS8CuAvnYYRM1wd5_mu6uno8OX8_cVD_Wm"

	// See documentation on defining a message payload.
	message := &messaging.Message{
		Notification: &messaging.Notification{
			Title: "Hey, TWNTY",
			Body:  "Whats up?",
		},
		Android: &messaging.AndroidConfig{
			Notification: &messaging.AndroidNotification{
				Icon:  "https://foo.bar.pizza-monster.png",
				Color: "#E63B2A",
			},
		},
		Token: registrationToken,
	}

	// Send a message to the device corresponding to the provided
	// registration token.
	response, err := client.Send(ctx, message)
	if err != nil {
		log.Fatalln(err)
	}
	// Response is a message ID string.
	fmt.Println("Successfully sent message:", response)
	// [END send_to_token_golang]
}
