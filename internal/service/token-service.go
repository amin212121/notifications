package service

import (
	"fmt"
	"redus-notifications/internal/repository"
)

type PostServiceI interface {
	Add(id string, token string)
	Remove(id string)
	Notify(id string, data string)
}

type TokenService struct {
	repo *repository.DeviceRepo
}

func NewTokenService(repo *repository.DeviceRepo) PostServiceI {
	return &TokenService{
		repo,
	}
}

func (t *TokenService) Add(id string, token string) {
	t.repo.Add(id, token)
}

func (t *TokenService) Remove(id string) {
	fmt.Println("removeToken")
}

func (t *TokenService) Notify(id string, data string) {
	t.repo.Notify("845", "adsads12u123128371278e12yg1")
}
