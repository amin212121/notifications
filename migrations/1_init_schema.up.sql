BEGIN;

CREATE table if not exists users_device(
    id SERIAL,
    device_token TEXT,
    user_id INTEGER
);

COMMIT;