package FCM

import (
	"context"
	firebase "firebase.google.com/go"
	"google.golang.org/api/option"
	"log"
)

func Run() *firebase.App {
	opt := option.WithCredentialsFile("./redus-bd1c1-firebase-adminsdk-557xc-661a7c3f3f.json")
	app, err := firebase.NewApp(context.Background(), nil, opt)
	if err != nil {
		log.Fatalf("error initializing app: %v\n", err)
	}
	return app
}
